import React, { Component } from 'react';
import PropTypes from 'prop-types';


export class Lable extends Component {

	render(){
    let {name, type, placeholder, value, children} = this.props;

    return(
      <div className="myLable">
        <div> {name} </div>
        <div className="myLableContainer">
          {
            React.Children.map(
              children,
              (ChildrenItem) => {
                    return React.cloneElement(ChildrenItem, {
                    name: ChildrenItem.props.name,
                  })
              }
            )
            }
        </div>
      </div>
    )

  }
}



export const LableItem = ({name, type, placeholder, value}) => {

  return(
    <div>
      <label>
        <div>Задание 2</div>
        <input
          name={name}
          type={type}
          placeholder={placeholder}
          value={value}
          onChange={this.handleFormChange}          
          autocomplite="false"
        />
      </label>      
    </div>
  );
};