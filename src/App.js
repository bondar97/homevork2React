import React, { Component } from 'react';
import './App.css';
import {Toggler, TogglerItem} from './toggler';
import {Lable, LableItem} from './myComponent';

// import CompWithPropTypes from './compWithPropTypes';


class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      data: {
        name: "",
        password: "",
        lang: "",
        number: "",
        activeToggler: "left",
        activeTogglerGender: "male",
        send: false,
        placeholder: "",
        type: "",
        value: ""
      }
    };
  }

  changeStatus = (event) => {
    let TogglerValue = event.target.innerText;
    this.setState({
      data:{
        ...this.state.data,
        activeToggler: TogglerValue
      }});
  }

  changeStatusGender = (event) => {
    let TogglerValue = event.target.innerText;
    this.setState({
      data:{
        ...this.state.data,
        activeTogglerGender: TogglerValue
      }});
  }
  handleSubmit = (event) => {
    event.preventDefault();
       this.setState({
      data:{
        ...this.state.data,
        send: true
      }});
    console.log('submitted data',this.state.data);
  }
  handleFormChange = event => {
    let value = event.target.value;
    let name = event.target.name;
    console.log(name, value);

    this.setState({
      data:{
        ...this.state.data,
        [name]: value
      }});
  }

  render() {
    let {value} = this.state.data;
    let {activeToggler} = this.state.data;
    let {activeTogglerGender} = this.state.data;

    return (
      <div className="App">
        <h1>My Form Component</h1>


        <form onSubmit={this.handleSubmit} autoComplete="off">
          <label>
           <div>User name</div>
            <input
             type="text"
             name="name"
             onChange={this.handleFormChange}
             autocomplite="false"
            />
          </label>
          <label>
            <div>Password</div>
            <input
              type="password"
              name="password"
              onChange={this.handleFormChange}
            />
          </label>
          <label>
            <div>AGE</div>
            <input
              type="number"
              name="age"
              onChange={this.handleFormChange}
            />
          </label>
          <label>
            <div>Favorite programming language</div>
            <input
              type="text"
              name="lang"
              onChange={this.handleFormChange}
            />
          </label>
          <Toggler
            name="Choose layout"
            activeToggler={activeToggler}
            changeStatus={this.changeStatus}
          >
            <TogglerItem name="left"/>
            <TogglerItem name="center"/>
            <TogglerItem name="right"/>
            <TogglerItem name="baseline"/>
          </Toggler>

        <Toggler
          name="Choose gender"
          activeToggler={activeTogglerGender}
          changeStatus={this.changeStatusGender}
        >
          <TogglerItem name="male"/>
          <TogglerItem name="female"/>
        </Toggler>

        <Lable>
          <LableItem
            name="lable"
            type="text"
            placeholder="placeholder" 
            onChange={this.handleFormChange} 
          />
        </Lable>

        <button type="submit">Send</button>
        </form>
      </div>
    );
  }
}

export default App;
